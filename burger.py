#!python

"""burger.py

Usage: burger.py [-v...]
  burger.py CODE [-v...]
  burger.py -h | --help

Options:
-v --verbose                print verbose status messages
-h --help                   Show this screen

"""

import logging
import sys
from time import sleep

from bs4 import BeautifulSoup

from docopt import docopt

import requests

if sys.version_info > (3, 0):
    import http.client as http_client


forms = [
    ["Did you visit", None,
     "YesNoASCQuestion", "Yes", "R000060", "1"],

    ["Please select your order type", None,
     "radio", "Carry out", "R004000", "1"],

    ["Please rate your overall satisfaction with your experience", None,
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R001000", "4"],

    ["Please rate your satisfaction with", "The quality of your food",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R028000", "4"],
    ["Please rate your satisfaction with", "The accuracy of your order.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R007000", "4"],
    ["Please rate your satisfaction with", "The taste of your food.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R005000", "4"],
    ["Please rate your satisfaction with", "The variety of menu items.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R000062", "4"],
    ["Please rate your satisfaction with", "The temperature of your food.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R006000", "4"],
    ["Please rate your satisfaction with", "The taste of the French",
     "HighlySatisfiedNeitherDESCQuestion", "Highly Satisfied", "R000285", "5"],
    ["Please rate your satisfaction with", "The cleanliness of the restroom",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R013000", "4"],
    ["Please rate your satisfaction with", "The interior cleanliness",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R073000", "4"],
    ["Please rate your satisfaction with", "The exterior cleanliness",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R074000", "4"],
    ["Please rate your satisfaction with", "The speed of service.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R008000", "4"],
    ["Please rate your satisfaction with", "The ease of placing your order.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R011000", "4"],
    ["Please rate your satisfaction with", "The friendliness of the crew.",
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R009000", "4"],

    ["The overall value for the price you paid", None,
     "HighlySatisfiedNeitherDESCQuestion", "Satisfied", "R015000", "4"],

    ["Did you customize or have a special request for your sandwich", None,
     "radio", "No", "R000103", "3"],

    ["Please select the main item you ordered", None,
     "radio", "Chicken sandwich", "R000225", "2"],
    ["Please select the main item you ordered", None,
     "radio", "Other", "R000225Other", ""],

    ["Please rate your satisfaction with the chicken sandwich", None,
     "radio", "Satisfied", "R000274", "4"],

    ["Did you purchase an item from the Signature Crafted", None,
     None, "No", "R000276", "2"],

    ["What type of food did you order", None,
     "radio", "Lunch/dinner", "R000156", "2"],

    ["Did you experience a problem with your visit", None,
     None, "No", "R016000", "2"],

    ["Based on this visit, what is the likelihood", "Recommend this",
     None, "Likely", "R019000", "4"],
    ["Based on this visit, what is the likelihood", "Return to this",
     None, "Likely", "R018000", "5"],


    ["Please tell us in three or more sentences why you were not Highly", None,
     None, None, None, None],

    ["Did you order from the McPick menu", None,
     None, "No", "R000279", "2"],

    ["Did you use the McDonald", None,
     None, "No", "R000289", "2"],

    ["Thank you for your feedback", "How many people",
     None, "Just yourself", "R060000", "1"],
    ["Thank you for your feedback", "Including this visit, how many times",
     None, "One", "R020000", "1"],

    ["Compared to other competitors in this area", None,
     None, "Somewhat Better", "R000034", "4"],

    ["Which of the following fast food restaurants", None,
     None, "Wendy", "R058140", "1"],

    ["Which of the following fast food restaurants", None,
     None, "Wendy", "R059000", "15"],

    ["Were you given clear instructions", None,
     None, "Yes", "R000298", "1"],
    ["Did your order taker identify the order number", None,
     None, "Yes", "R000297", "1"],
    ["When you picked up your order, was it repeated back to you", None,
     None, "Yes", "R000299", "1"],
    ["When did you receive your drink", "When you placed your order",
     None, "--When I placed my order--", "R000300", "1"],
    ["After placing your order which did you do", None,
     None, "--waited by the counter--", "R000301", "2"],
    ["Did you receive what you ordered", None,
     None, "Yes", "R000224", "1"],
    ["Did you notice the digital screen on the menu board", None,
     None, "Yes", "R000302", "2"],
]


def burger(args):
    verbose = args['--verbose']
    if verbose > 1:
        print(args)

    logging.basicConfig(level=logging.WARN)
    logger = logging.getLogger('burger')

    if verbose:
        # logging.getLogger().setLevel(logging.INFO)
        logger.setLevel(logging.INFO)
    if verbose > 1:
        # logging.getLogger().setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    if verbose > 3:
        http_client.HTTPConnection.debuglevel = 1
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True

    if 'CODE' in args and args['CODE']:
        code = args['CODE']
    else:
        code = "10515-02980-42517-18158-00027-3"
    codons = code.split("-")
    if len(codons) != 6:
        raise ValueError(
            "CODE {} should be of the form AAAAA-BBBBB-CCCCC-DDDDD-EEEEE-F"
            .format(code))

    mcd = "https://www.mcdvoice.com"
    headers = {}
    history = []
    responses = []
    url = mcd

    u = requests.session()

    logger.info("Get {}".format(url))

    history.append([url, []])
    r = u.get(url)
    responses.append(r)
    rn = 0

    fields = {}
    for screen, question, qtype, answer, field, value in forms:
        fields[field] = value

    n = 1
    while n < 30:
        soup = BeautifulSoup(r.text, 'html.parser')
        form = soup.find('form')
        if form:
            formid = form['id']
            action = form['action']
            payload = {}
            for hidden in form.find_all(type="hidden"):
                payload[hidden['name']] = hidden['value']

            if formid == 'surveyEntryForm':
                entryPayload = dict(zip(["cn%s" % i for i in range(1, 7)],
                                        codons))
                payload.update(entryPayload)
                payload['JavaScriptEnabled'] = '1'
            elif formid == 'finishForm':
                valcode = form.find(class_="ValCode")
                if valcode:
                    return valcode.string
                else:
                    logger.error("End of forms, but no value code found!")
                    logger.error(form)
            else:
                for field in form.find_all("input"):
                    if verbose >= 2:
                        logger.debug("field {}".format(field))
                        logger.debug("  attrs {}".format(field.attrs))
                    if 'type' in field.attrs \
                       and field['type'] != 'hidden' \
                       and 'name' in field.attrs:
                        name = field['name']
                        if verbose >= 2:
                            logger.debug("processing field {}".format(field))
                        if name not in fields:
                            if verbose >= 2:
                                logger.warning(
                                    "field {} not found in form\n{}"
                                    .format(name, form))
                        else:
                            payload[name] = fields[name]
                    else:
                        if verbose >= 2:
                            logger.debug("skipping field {}".format(field))
                            logger.debug("  attrs {}".format(field.attrs))
                            # pdb.set_trace()

            if verbose >= 2:
                logger.debug("---input fields---")
                for inp in form.find_all("input"):
                    logger.debug(inp)
        else:
            logger.error("on pass {} no form was found.".format(n))
            break

        logger.debug("{} response: form goes to {}:".format(n, url))

        headers = {'referer': r.request.url}
        url = mcd + "/" + action
        n += 1
        if verbose >= 2:
            logger.debug("\n------------------------------\n")

        logger.info("{} posting {} to {}".format(n, url, payload))

        sleep(0.5)
        history.append([url, payload])
        r = u.post(url, data=payload, headers=headers)
        responses.append(r)
        if verbose > 2:
            logger.debug(r.text)
        rn += 1

    return None


if __name__ == "__main__":

    args = docopt(__doc__)
    valcode = burger(args)

    print(valcode)
